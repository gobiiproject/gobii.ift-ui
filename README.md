# sift

Basic webpage for the Gobii instruction file translator

## Overview

## Deployment

We will be using [leiningen](https://leiningen.org/) to build the project, so make sure you have it installed.

Next we will build an uberjar using:
```bash
lein uberjar
```

This will put two uberjars in the target folder; go ahead and grab the `-standalone` version and move it to where you want to run the site.

After this, it's as simple as running the jar; optionally, you can specify a port as it's only argument (defaults to 80):

```bash
java -jar sift-{version}-standalone.jar {port_number}
```

That's it! Everything should be up and running now.
