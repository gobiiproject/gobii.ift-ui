(ns sift.core
  (:require
   [ring.adapter.jetty :refer [run-jetty]]
   [ring.util.response :as response]
   [reitit.ring :as ring]
   [clojure.pprint :refer [pprint]])
  (:gen-class))

(def app
  (ring/ring-handler
    (ring/router
      [["/"
        (fn [& _] (response/resource-response "index.html" {:root "public"}))]
       ["/favicon.ico "
        (fn [& _] (response/resource-response "favicon.ico" {:root ""}))]
       ["/*" (ring/create-resource-handler)]]
      {:conflicts (constantly nil)})))


(defn start
  [port]
  (run-jetty #'app {:join? false, :port port}))

(defn -main
  [& [port]]
  (start (if port (Long. port) 80)))
