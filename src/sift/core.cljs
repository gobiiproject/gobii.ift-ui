(ns sift.core
  (:require
   [ift.core :as ift]
   [reagent.core :as r]
   [reagent.dom :as d]))

(enable-console-print!)

;; define your app data so that it doesn't get over-written on reload

(defonce app-state (r/atom {:from "Translate from version..."
                            :to "Translate to version..."
                            :type "Instruction Type"
                            :input ""
                            :output ""}))


; (add-watch app-state :asdf (fn [k r o n] (println n)))

(defn base
  []
  [:div.root
   [:div.controls

    (into
     [:select.type {:on-change #(swap! app-state assoc :type (keyword (.-value (.-target %))))
                    :default-value (:type @app-state)}
      [:option {:disabled true} "Instruction Type"]]
     (map (fn [ty] [:option {:value ty} (name ty)]) (keys ift/protocols)))

    [:button.translate
     {:on-click #(let [{:keys [type from to input]} @app-state]
                   (swap! app-state assoc :output
                          (try
                            (ift/translate type from to input))))}
                            ; (catch js/Error e
                            ;   (.-message e)))))}
     "Translate!"]]

   [:div.left
    (into
     [:select.from {:on-change #(swap! app-state assoc :from (keyword (.-value (.-target %))))
                    :default-value (:from @app-state)}
      [:option {:disabled true} "Translate from version..."]]
     (map (fn [k] [:option {:value k} (name k)])
          (map first
               (filter (fn [[k v]] (contains? v :parse))
                       (get ift/protocols (:type @app-state))))))
    [:textarea#input {:value (:input @app-state)
                      :on-change #(swap! app-state assoc :input (.-value (.-target %)))}]]

   [:div.right
    (into
     [:select.to {:on-change #(swap! app-state assoc :to (keyword (.-value (.-target %))))
                  :default-value (:to @app-state)}
      [:option {:disabled true} "Translate to version..."]]
     (map (fn [k] [:option {:value k} (name k)])
          (map first
               (filter (fn [[k v]] (contains? v :translate))
                       (get ift/protocols (:type @app-state))))))
    [:textarea#output {:on-change #(swap! app-state assoc :output (.-value (.-target %)))
                       :value (:output @app-state)}]]])

(defn main
  []
  (d/render [base] (.. js/document (getElementById "app"))))

(main)
